## To do list:

* Style recent articles at /projects/"article"
* Style highlight, make quote etc.
* Create better functionality for pulling the images. Check Stoyan's idea
* Style the mobile article version!
* Pick images and update the website.
* Draft some blog posts!
* Check for localization!